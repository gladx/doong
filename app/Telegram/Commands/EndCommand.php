<?php

namespace App\Telegram\Commands;

use Mongo;
use Telegram;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

class EndCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'end';

    /**
     * @var string Command Description
     */
    protected $description = 'پایان یا توقف دستور';

    /**
     * @inheritdoc
     */
    public function handle()
    {
        $update = Telegram::getWebhookUpdates();
        $tg_user = $update->getMessage()->getFrom();
        $user = Mongo::get()->doong->Users->findOne(['tg_id' => $tg_user->getId()]);

        Mongo::get()->doong->Users->updateOne(['tg_id' => $tg_user->getId()], ['$set' => ['status' => 'none']]);

        $this->replyWithMessage(['text' => 'پایان دستور']);
    }
}
