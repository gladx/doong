<?php

namespace App\Telegram\Commands;

use Telegram;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Mongo;

class RegisterCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'register';

    /**
     * @var string Command Description
     */
    protected $description = 'ثبت نام';

    /**
     * @inheritdoc
     */
    public function handle()
    {
        $update = Telegram::getWebhookUpdates();
        $chatId = $update->getMessage()->getChat()->getId();
        $user = $update->getMessage()->getFrom();

        // Check user exist
        $collection = Mongo::get()->doong->Users;
        $result = $collection->findOne(['tg_id' => $user->getId()]);

        // Add User if not exist

        if ($result === null) {
            $collection->insertOne([
                'tg_id' => $user->getId(),
                'tg_first_name' => $user->getFirstName(),
                'tg_last_name' => $user->getLastName(),
                'tg_user_name' => $user->getUsername(),
                'date' => time()
            ]);
        }

        Telegram::sendMessage([
            'chat_id' => $chatId,
            'text' => 'خوش آمدید',
        ]);
    }
}
