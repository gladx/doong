<?php

namespace App\Telegram\Commands;

use Mongo;
use Telegram;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

class StartCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'start';

    /**
     * @var string Command Description
     */
    protected $description = 'شروع';

    /**
     * @inheritdoc
     */
    public function handle()
    {
        $this->replyWithChatAction(['action' => Actions::TYPING]);

        $update = Telegram::getWebhookUpdates();
        $tg_user = $update->getMessage()->getFrom();
        $user = Mongo::get()->doong->Users->findOne(['tg_id' => $tg_user->getId()]);

        Mongo::get()->doong->Users->updateOne(['tg_id' => $tg_user->getId()], ['$set' => ['status' => 'none']]);

        $this->replyWithMessage(['text' => 'سلام خوش آمدید']);
        // $this->triggerCommand('subscribe');
    }
}
