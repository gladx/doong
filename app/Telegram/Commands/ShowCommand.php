<?php

namespace App\Telegram\Commands;

use Mongo;
use Telegram;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Api;

class ShowCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'show';

    /**
     * @var string Command Description
     */
    protected $description = 'نمایش دنگ‌ها';

    protected $update;
    protected $user;
    protected $tg_user;

    /**
     * @inheritdoc
     */
    public function handle()
    {
        $update = Telegram::getWebhookUpdates();

        $user = Mongo::get()->doong->Users->findOne([
            'tg_id' => $update->getMessage()->getFrom()->getId()
        ]);
        $doongs = Mongo::get()->doong->Doongs->find(['owner_id' => $user->_id]);

        $str = '';
        foreach ($doongs as $doong) {
            $str .= '/#1 ' . $doong['name'] . "\n";
        }

        Telegram::sendMessage([
            'chat_id' => $update->getMessage()->getChat()->getId(),
            'text' => $str,
        ]);
    }
}
