<?php

namespace App\Telegram\Commands;

use Mongo;
use Telegram;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Api;

class NewCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'new';

    /**
     * @var string Command Description
     */
    protected $description = 'ایجاد دنگ جدید';

    protected $update;
    protected $user;
    protected $tg_user;

    /**
     * @inheritdoc
     */
    public function handle()
    {
        $this->update = Telegram::getWebhookUpdates();
        $this->tg_user = $this->update->getMessage()->getFrom();
        $this->user = Mongo::get()->doong->Users->findOne(['tg_id' => $this->tg_user->getId()]);

        $this->replyWithChatAction(['action' => Actions::TYPING]);

        return $this->createNew();
    }

    public function createNew()
    {
        //Find all Doong with temp status and remove all
        $r = Mongo::get()->doong->Doongs->deleteMany(['owner_id' => $this->user->_id, 'status' => 'temp']);

        $doong = Mongo::get()->doong->Doongs->insertOne([
            'owner_id' => $this->user->_id,
            'name' => 'Untitle',
            'status' => 'temp',
            'participants' => []
        ]);

        Mongo::get()->doong->Users->updateOne(['tg_id' => $this->tg_user->getId()], ['$set' => ['status' => 'setDoongName', 'currentDoong' => $doong->getInsertedId()]]);

        return Telegram::sendMessage([
            'chat_id' => $this->update->getMessage()->getChat()->getId(),
            'text' => 'برای دنگ یک نام انتخاب کنید',
        ]);
    }
}
