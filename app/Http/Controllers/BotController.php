<?php

namespace App\Http\Controllers;

use Mongo;
use Telegram;
use Telegram\Bot\Keyboard\Keyboard;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class BotController extends Controller
{
    public function index()
    {
        return $response = Telegram::getMe();

        $botId = $response->getId();
        $firstName = $response->getFirstName();
        $username = $response->getUsername();
    }

    public function update()
    {
        return $response = Telegram::getUpdates();
    }

    public function webhook()
    {
        $update = Telegram::commandsHandler(true);
        $this->writeLog($update);

        $user = Mongo::get()->doong->Users->findOne([
            'tg_id' => $update->getMessage()->getFrom()->getId()
        ]);

        $message = $update->getMessage();

        if ($message !== null && $message->has('text') && strpos($message->getText(), '/') !== 0) {
            $status = $user->status ?? 'none';

            if (method_exists($this, $status)) {
                $this->$status($user, $update);
            }
        }

        return new JsonResponse('ok', 200);
    }

    public function getUsers()
    {
        $collection = Mongo::get()->doong->Users;

        return $collection->find()->toArray();
    }

    public function setDoongName($user, $update)
    {
        Mongo::get()->doong->Doongs->updateOne(
            ['_id' => $user->currentDoong, 'status' => 'temp'],
            ['$set' => ['status' => 'none', 'name' => $update->getMessage()->getText()]]
        );

        Telegram::sendMessage([
            'chat_id' => $update->getMessage()->getChat()->getId(),
            'text' => 'کاربران را وارد کنید',
        ]);

        Mongo::get()->doong->Users->updateOne(['tg_id' => $update->getMessage()->getFrom()->getId()], ['$set' => ['status' => 'addDongParticipant']]);
    }

    public function addDongParticipant($user, $update)
    {
        // if user exists retrun
        $r = Mongo::get()->doong->Doongs->findOne(['_id' => $user->currentDoong, 'participants.name' => $update->getMessage()->getText()]);

        if ($r) {
            return Telegram::sendMessage([
                'chat_id' => $update->getMessage()->getChat()->getId(),
                'text' => 'نام تکراری است نام دیگری را انتخاب کنید',
            ]);
        }

        //Get Count participants
        $r = Mongo::get()->doong->Doongs->findOne(['_id' => $user->currentDoong], ['projection' => ['participants' => 1, '_id' => 0]]);
        $participants = $r->participants;
        $countParticipants = count($participants);

        Mongo::get()->doong->Doongs->updateOne(
            ['_id' => $user->currentDoong],
            [
                '$push' => [
                    'participants' => [
                        'user_index' => ($countParticipants + 1),
                        'name' => $update->getMessage()->getText(),
                        'is_admin' => false
                    ]
                ]
            ]
        );

        //Get All user name
        $r = Mongo::get()->doong->Doongs->findOne(['_id' => $user->currentDoong], ['projection' => ['participants' => 1, '_id' => 0]]);
        $participants = $r->participants;
        $i = 1;
        $str = 'لیست کاربران' . "\n";
        foreach ($participants as $p) {
            $str .= $i . ' ' . $p['name'] . "\n";
            $i++;
        }

        Telegram::sendMessage([
            'chat_id' => $update->getMessage()->getChat()->getId(),
            'text' => $str . 'کاربر دیگری را وارد کنید یا پایان را بزنید /end ',
            'parse_mode' => 'html',
            'disable_notification' => true,
            'reply_markup' => Keyboard::make()
                ->inline()
                ->row(
                    Keyboard::inlineButton(['text' => 'add +', 'callback_data' => 'New:addUser'])
                )
        ]);
    }

    public function writeLog($data)
    {
        if (!$this->write_file(storage_path() . '/logs/tmp_message/message-' . time() . '.json', json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE))) {
            echo 'Unable to write the file';
        }
    }

    /**
     * Write File
     *
     * Writes data to the file specified in the path.
     * Creates a new file if non-existent.
     *
     * @param	string	$path	File path
     * @param	string	$data	Data to write
     * @param	string	$mode	fopen() mode (default: 'wb')
     * @return	bool
     */
    public function write_file($path, $data, $mode = 'wb')
    {
        if (!$fp = @fopen($path, $mode)) {
            return false;
        }

        flock($fp, LOCK_EX);

        for ($result = $written = 0, $length = strlen($data); $written < $length; $written += $result) {
            if (($result = fwrite($fp, substr($data, $written))) === false) {
                break;
            }
        }

        flock($fp, LOCK_UN);
        fclose($fp);

        return is_int($result);
    }
}
